var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 14,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        }
    ]
}
$(document).ready(function(){
    getCoursePopular();
    getCourseTrending();
});
function getCoursePopular () {
    for (var bI in gCoursesDB.courses){
        var vCoursePopular = "";
        if (gCoursesDB.courses[bI].isPopular == true){
            vCoursePopular += `
            <div class="col-3">
                <div class="card" style="width: 16rem; ">
                    <img class="rounded-top" src="${gCoursesDB.courses[bI].coverImage}">
                <div class="card-body">
                    <a class="card-title btn btn-link text-left" style="height: 75px" href="#">${gCoursesDB.courses[bI].courseName}</a>
                    <p class="card-text"><i class="far fa-clock"></i>${gCoursesDB.courses[bI].duration} ${gCoursesDB.courses[bI].level}<br><b>${gCoursesDB.courses[bI].discountPrice}</b><del> ${gCoursesDB.courses[bI].price}</del></p>
                </div>
                <div class="card-footer">
                    <img style="width: 15%;" class="rounded-circle float-left" src="${gCoursesDB.courses[bI].teacherPhoto}" alt="">
                    <p class="pl-5">${gCoursesDB.courses[bI].teacherName}</p>
                    <i class="far fa-bookmark float-right fa-lg"></i>
                </div>
                </div>
            </div>
            `;
        }
        $($(".course-popular")).append(vCoursePopular);
    }
}
function getCourseTrending () {
    for (var bI in gCoursesDB.courses){
        var vCourseTrending = "";
        if (gCoursesDB.courses[bI].isTrending == true){
            vCourseTrending += `
            <div class="col-3">
                <div class="card" style="width: 16rem; ">
                    <img class="rounded-top" src="${gCoursesDB.courses[bI].coverImage}">
                <div class="card-body">
                    <a class="card-title btn btn-link text-left" style="height: 75px" href="#">${gCoursesDB.courses[bI].courseName}</a>
                    <p class="card-text"><i class="far fa-clock"></i>${gCoursesDB.courses[bI].duration} ${gCoursesDB.courses[bI].level}<br><b>${gCoursesDB.courses[bI].discountPrice}</b><del> ${gCoursesDB.courses[bI].price}</del></p>
                </div>
                <div class="card-footer">
                    <img style="width: 15%;" class="rounded-circle float-left" src="${gCoursesDB.courses[bI].teacherPhoto}" alt="">
                    <p class="pl-5">${gCoursesDB.courses[bI].teacherName}</p>
                    <i class="far fa-bookmark float-right fa-lg"></i>
                </div>
                </div>
            </div>
            `;
        }
        $($(".course-trending")).append(vCourseTrending);
    }
}